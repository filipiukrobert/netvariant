var SCROLL_WIDTH = 17;

if(isTouchDevice()){
	SCROLL_WIDTH = 0;
}

function isTouchDevice() {
    return 'ontouchstart' in document.documentElement;
}

window.addEventListener("load", function() {

	var body = document.querySelector("body");
	var body_content = document.getElementById("body_content");
	var dimmer = document.getElementById("dimmer");

	body.style.position = "static";
	body_content.style.display = "block";
	dimmer.style.display = "none";

});