var lat = 52.402478;
var lng = 16.909748;
var wspolrzedne = new google.maps.LatLng(lat, lng);

var my_coordinates = new google.maps.LatLng(24.711992, 46.670077);

var loaded=false;

var mapa;
var marker;

var opcjeMarkera;

var style =

[
{
	"featureType": "landscape",
	"stylers": [
	{ "visibility": "on" },
	{ "color": "#20b5e4" }
	]
},{
	"featureType": "poi",
	"elementType": "geometry",
	"stylers": [
	{ "color": "#1eb5e4" }
	]
},{
	"featureType": "road",
	"stylers": [
	{ "color": "#0fa2cf" }
	]
},{
	"featureType": "road",
	"elementType": "labels.text.fill",
	"stylers": [
	{ "visibility": "on" },
	{ "color": "#ffffff" }
	]
},{
	"featureType": "transit",
	"stylers": [
	{ "color": "#008bb5" }
	]
},{
	"featureType": "water",
	"stylers": [
	{ "color": "#5ad8ff" }
	]
},{
	"featureType": "administrative.country",
	"stylers": [
	{ "color": "#808080" },
	{ "visibility": "simplified" }
	]
},{
	"featureType": "administrative.province",
	"stylers": [
	{ "visibility": "on" },
	{ "invert_lightness": true },
	{ "weight": 0.5 }
	]
},{
	"featureType": "administrative.locality",
	"stylers": [
	{ "visibility": "on" }
	]
}
]

;

var my_style = 
[
  {
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#f5f5f5"
      }
    ]
  },
  {
    "elementType": "labels.icon",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#616161"
      }
    ]
  },
  {
    "elementType": "labels.text.stroke",
    "stylers": [
      {
        "color": "#f5f5f5"
      }
    ]
  },
  {
    "featureType": "poi",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "featureType": "poi",
    "elementType": "geometry",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "featureType": "poi.park",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#9e9e9e"
      }
    ]
  },
  {
    "featureType": "road",
    "elementType": "geometry.stroke",
    "stylers": [
      {
        "color": "#e8e8e8"
      }
    ]
  },
  {
    "featureType": "road.highway",
    "elementType": "geometry.fill",
    "stylers": [
      {
        "color": "#e0e0e0"
      }
    ]
  },
  {
    "featureType": "road",
    "elementType": "labels.icon",
    "stylers": [
      {
        "visibility":"on"
      },
      {
      	"saturation":"-100"
      }
    ]
  },
  {
    "featureType": "road",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color":"#686868"
      }
    ]
  },
  {
    "featureType": "transit",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "featureType": "transit.line",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#e5e5e5"
      }
    ]
  },
  {
    "featureType": "transit.station",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#eeeeee"
      }
    ]
  },
  {
    "featureType": "water",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#c9c9c9"
      }
    ]
  }
];

var opcjeMapy = { zoom: 17, center: my_coordinates, mapTypeId: google.maps.MapTypeId.ROADMAP, disableDefaultUI: true, navigationControl: false, styles: my_style };

function init_map()   
{   
	mapa = new google.maps.Map(document.getElementById("mapa"), opcjeMapy); 
	loaded=true;

	marker = new google.maps.Marker({
		position: my_coordinates,
		icon: "../img/icons/map_marker.png",
		map: mapa
	});
}   

document.addEventListener("DOMContentLoaded", function() {
	console.log("document DOMContentLoaded ready");
	init_map();
});
