
var loupe_button = document.getElementsByClassName("search_wrapper button")[0];
var heart_button = document.getElementsByClassName("social button")[0];
var language_button = document.getElementsByClassName("language button")[0];
var profile_button = document.getElementsByClassName("profile button")[0];
var primary_menu_hamburger_menu_button = document.getElementsByClassName("mobile_menu button")[0];

var fb_dropdown_button = document.getElementsByClassName("fb dropdown_item")[0];
var twitter_dropdown_button = document.getElementsByClassName("twitter dropdown_item")[0];
var google_plus_dropdown_button = document.getElementsByClassName("google_plus dropdown_item")[0];

var secondary_menu_content = document.getElementById("secondary_menu_content");


var buttons = [
	
	loupe_button,
	heart_button,
	language_button,
	profile_button,

	fb_dropdown_button,
	twitter_dropdown_button,
	google_plus_dropdown_button,

	primary_menu_hamburger_menu_button,
	secondary_menu_content
];

var active_icons_dir = "/active_icons";

buttons.forEach(function(item, index) {
	buttons[index].onmouseenter = function() {
		hoverIcon(buttons[index].getElementsByTagName("img")[0]);
	}

	buttons[index].onmouseleave = function() {
		unhoverIcon(buttons[index].getElementsByTagName("img")[0]);
	}
});

function hoverIcon(item) {

	var item_src = item.getAttribute("src");
	var slash_dir_index = item_src.lastIndexOf("/");

	var start = item_src.slice(0, slash_dir_index);
	var end = item_src.slice(slash_dir_index, item_src.length);

	var new_item_src = start + active_icons_dir + end;
	console.log(new_item_src);
	item.setAttribute("src", new_item_src);
}

function unhoverIcon(item) {
	
	var item_src = item.getAttribute("src");
	var active_dir_index = item_src.indexOf(active_icons_dir);

	var start = item_src.slice(0, active_dir_index);
	var end = item_src.slice(active_dir_index + active_icons_dir.length, item_src.length);

	var new_item_src = start + end;
	console.log(new_item_src);
	item.setAttribute("src", new_item_src);
}