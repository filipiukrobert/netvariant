

var search_button = document.querySelector("#primary_menu .search_wrapper");
var search_bar = document.querySelector("#search_bar");

var makred_search = document.querySelectorAll("[data-mark='search']");

search_button.onmouseover = function() {
	search_bar.style.display = "block";
}

search_button.onmouseout = function() {
	search_bar.style.display = "none";
}

// ****************************************

var about_us_link = document.querySelector(".left_menu .link[data-mark='about_us'] > a");
var about_us_menu = document.querySelector(".left_menu .about_us_menu");

var marked_about_us_leave = document.querySelectorAll(".left_menu [data-mark='about_us_leave']");

var boxShadow;

console.log(about_us_link, marked_about_us_leave);

	about_us_link.onmouseover = function() {
		console.log("about_us_link onmouseover");
		about_us_menu.style.display = "block";
		console.log(about_us_menu.style.display);
		boxShadow = about_us_menu.style.boxShadow;
		about_us_menu.style.boxShadow = "none";

		for(var j = 0; j < marked_about_us_leave.length; j++) {

		console.log(marked_about_us_leave[j]);

		marked_about_us_leave[j].onmouseleave = function() {
			about_us_menu.style.display = "none";
			about_us_menu.style.boxShadow = boxShadow;
			about_us_active = false;

		}
	}
}


var about_us_link_mobile = document.querySelector(".left_mobile_menu .link[data-mark='about_us'] > a");
var about_us_menu_mobile = document.querySelector(".left_mobile_menu .about_us_menu");

var marked_about_us_leave_mobile = document.querySelectorAll(".left_mobile_menu [data-mark='about_us_leave']");

var boxShadow_mobile;

console.log(about_us_link_mobile, marked_about_us_leave_mobile);

	about_us_link_mobile.onmouseover = function() {
		console.log("about_us_link_mobile onmouseover");
		about_us_menu_mobile.style.display = "flex";
		console.log(about_us_menu_mobile.style.display);
		boxShadow = about_us_menu_mobile.style.boxShadow;
		about_us_menu_mobile.style.boxShadow = "none";

		for(var j = 0; j < marked_about_us_leave_mobile.length; j++) {

		console.log(marked_about_us_leave_mobile[j]);

		marked_about_us_leave_mobile[j].onmouseleave = function() {
			about_us_menu_mobile.style.display = "none";
			about_us_menu_mobile.style.boxShadow = boxShadow;
			about_us_active = false;

		}
	}
}


// ***************************************************

var mobile_menu = document.querySelector(".mobile_menu.button");
var left_mobile_menu = document.querySelector(".left_mobile_menu");
var left_menu = document.querySelector("#primary_menu > .left_menu");

var makred_mobile_menu = document.querySelectorAll("[data-mark='mobile_menu']");

mobile_menu.onmouseover = function() {
	left_mobile_menu.style.display = "flex";
}

for(var i = 0, elem = makred_mobile_menu; i < makred_mobile_menu.length; i++) {
	
	elem[i].onmousemove = function() {
		left_mobile_menu.style.display = "flex";
	}

	elem[i].onmouseout = function() {
		left_mobile_menu.style.display = "none";
	}
}


// ***************************************************
// RWD

var secondary_menu = document.querySelector("#secondary_menu");
var secondary_menu_content = secondary_menu.querySelector("#secondary_menu_content");

secondary_menu.style.width = ( (window.innerWidth - SCROLL_WIDTH) - (71 * 2) - (33 * 2) ) + "px";
secondary_menu_content.style.width = ( (window.innerWidth - SCROLL_WIDTH) - (71 * 2) - (33 * 2) ) + "px";

window.addEventListener("resize", function() {
	secondary_menu.style.width = ( (window.innerWidth - SCROLL_WIDTH) - (71 * 2) - (33 * 2) ) + "px";
	secondary_menu_content.style.width = ( (window.innerWidth - SCROLL_WIDTH) - (71 * 2) - (33 * 2) ) + "px";
});

document.addEventListener("scroll", function() {

	sticky(secondary_menu_content, secondary_menu);
});

function sticky(stickyElem, origin) {

	var top_sticky = stickyElem.getBoundingClientRect().top;
	var top_origin = origin.getBoundingClientRect().top;

	if( top_sticky <= 0 ) {
		stickyElem.style.position = "fixed";
	}

	if( top_origin >= 0 ) {
		stickyElem.style.position = "relative";
	}

}

// ***************************************************
