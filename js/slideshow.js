
var slides = document.querySelectorAll(".slides .slide");
console.log(slides);

resizeSlides(slides);

window.onresize = function() {
	resizeSlides(slides);
}

function resizeSlides(slides) {
	var widthWithoutScroll = window.innerWidth - SCROLL_WIDTH;

	for(var i = 0; i < slides.length; i++) {
		slides[i].style.width = widthWithoutScroll + "px";
	}
}

var slide_area = document.querySelector("#slideshow");

var slider = document.querySelector("#slideshow .slides");
var is_running = false;

var slide_left = -1;
var slide_right = 1;

var y0 = null;

var slide_position = 1;
var slide_count = slider.querySelectorAll(".slide").length;

if( isTouchDevice() ){

	slide_area.ontouchstart = function(event) {

		y0 = event.changedTouches[0].screenX;

		console.log("y0", event.changedTouches[0].screenX);

		slide_area.ontouchend = function(event) {

			var y1 = event.changedTouches[0].screenX;
			console.log("y1", event.changedTouches[0].screenX);

			if(!is_running) {
				if( y0 > y1 && slide_position < slide_count) {
					is_running = true;

					console.log("slide left");
					slide_position++;
					slide(slider, 1, slide_left);
				}

				if( y0 < y1 && slide_position > 1 ) {
					is_running = true;

					console.log("slide right");
					slide_position--;
					slide(slider, 1, slide_right);
				}

				console.log(y0,y1,slide_position, slide_count);

				slide_area.ontouchend = function() {};
			}

		}	
	}

} else {

	slide_area.onmousedown = function(event) {

		y0 = event.clientX;

		console.log("y0", event.clientX);

		slide_area.onmouseup = function(event) {

			console.log("y0", event.clientX);

			var y1 = event.clientX;

			if(!is_running) {
				if( y0 > y1 && slide_position < slide_count) {
					is_running = true;

					console.log("slide left");
					slide_position++;
					slide(slider, 1, slide_left);
				}

				if( y0 < y1 && slide_position > 1 ) {
					is_running = true;

					console.log("slide right");
					slide_position--;
					slide(slider, 1, slide_right);
				}

				console.log(y0,y1,slide_position, slide_count);

				slide_area.onmouseup = function() {};
			}

		}	
	}

}

function slide(slider, duration, direction) {
	
	var offset = parseInt(slider.style.left);
	console.log("offset pre", offset);

	if( isNaN(offset) ) {
		offset = 0;
	}

	var val = 0;
	var delta_max = 100;
	var id = setInterval(frame, duration);

	function frame() {
		if (val == delta_max) {
			is_running = false;
			clearInterval(id);
			offset += (100 * direction);
			slider.style.left = offset + "%";
			console.log("offset post", offset, "is_running", is_running);
		} else {
			val++;
			slider.style.left = (direction * val + offset) + '%'; 
		}
	}

}

/* ******************************************************* */

var slideshow_buttons_icon = document.querySelectorAll("#slideshow .left_button .content img.icon, #slideshow .right_button  .content img.icon");

console.log(slideshow_buttons_icon[0].getAttribute("src"));

var before_img_src;
var after_img_src = "img/icons/white_hand_move_icon.png";

var defaultMarginTop;
var defaultMarginOnSide;

for(var i = 0; i < slideshow_buttons_icon.length; i++) {

	slideshow_buttons_icon[i].onmouseover = function() {

		before_img_src = this.getAttribute("src");
		defaultMarginTop = this.style.marginTop;
		
		this.setAttribute("src", after_img_src);
		this.style.marginTop = "-21.5px";

		if( this.closest(".button").classList.contains("left_button") ) {
			defaultMarginOnSide = this.style.marginLeft;
			this.style.marginLeft = "6px";
		} else {
			defaultMarginOnSide = this.style.marginRight;
			this.style.marginRight = "6px";
		}

	}

	slideshow_buttons_icon[i].onmouseleave = function() {

		this.setAttribute("src", before_img_src);
		this.style.marginTop = defaultMarginTop;

		if( this.closest(".button").classList.contains("left_button") ) {
			this.style.marginLeft = defaultMarginOnSide;
		} else {
			this.style.marginRight = defaultMarginOnSide;
		}

	}
}